class PositionModel {
  final String lat, lng;

  PositionModel(this.lat, this.lng);
}