import 'package:flutter/material.dart';
import 'package:alvo_couro/src/pages/catalogo/catalogo-widget.dart';
import 'package:alvo_couro/src/pages/home/home-widget.dart';
import 'package:alvo_couro/src/pages/amostras/amostra-widget.dart';



class TelaInicial extends StatefulWidget {
 @override
 State<StatefulWidget> createState() {
    return _TelaInicial();
  }
}
class _TelaInicial extends State<TelaInicial> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Home(),
    CatalogoWidget(),
    AmostrarWidget()
 ];


 @override
 Widget build(BuildContext context) {
   return Scaffold(     
     body:  _children[_currentIndex],
     bottomNavigationBar: BottomNavigationBar(
       onTap: onTabTapped, // new
       currentIndex: _currentIndex, 
       items: [
         BottomNavigationBarItem(
           icon: new Icon(Icons.home),
           title: new Text('Home'),
         ),
         BottomNavigationBarItem(
           icon: new Icon(Icons.mail),
           title: new Text('Catalogos'),
         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.person),
           title: Text('Amostras')
         )
       ],
     ),
   );
 }
 void onTabTapped(int index) {
   setState(() {
     _currentIndex = index;
   });
 }
}