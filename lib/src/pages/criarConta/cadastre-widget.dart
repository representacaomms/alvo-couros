import 'package:alvo_couro/src/pages/criarConta/cadastre-bloc.dart';
import 'package:alvo_couro/src/pages/login/login-widget.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class CadastreWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CadastreBloc>(
      bloc: CadastreBloc(),
      child: Material(
              child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
         
             _CadastreContent()
          ],
        ),
      ),
    );
  }
}

class _CadastreContent extends StatelessWidget {


  
  @override
  Widget build(BuildContext context) {

    _bottoes(){
      return Column(
        children: <Widget>[
          Padding(
         padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            // The first text field will be focused as soon as the app starts
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o seu Email!',                
              ),              
              autofocus: true,
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o seu CNPJ!'
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o seu Telefone!'
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o seu CEP!'
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o numero da Empresa!'
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Insira o Segmento da Empresa!'
              ),
            ),
                        
          ],
        ),
          ),
          Padding(padding: EdgeInsets.only(top: 40)),
          RaisedButton.icon(
            textColor: Colors.white,
            color: Colors.lightGreen,
            icon: Icon(Icons.arrow_forward_ios), 
            label: Text("    Cadastrar    "), 
           onPressed: () {
              var alert = new AlertDialog(
                    title: new Text("Sucesso!"),
                    content: new Text("Cadastro realizado com sucesso!! "),
                    
                  );
                  showDialog(context: context, child: alert);
                  Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginWidget()),
              );
           },
          ),
        ],
      );
    }   

    return Column(
      
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
              
        Container(
          alignment: Alignment.center,
          child:  Text('Faça seu cadastro !', style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.brown
          ),),
          height: 120,
          ),
        _bottoes()
      ],
    );
  
  }
 
    
}

