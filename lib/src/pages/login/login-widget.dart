import 'package:alvo_couro/src/pages/criarConta/cadastre-widget.dart';
import 'package:alvo_couro/src/pages/login/login-bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:alvo_couro/src/pages/telaInicial/telaInicial-widget.dart';

class LoginWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      bloc: LoginBloc(),
      child: Material(
              child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
         
             _LoginContent()
          ],
        ),
      ),
    );
  }
}

class _LoginContent extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    _bottoes(){
      return Column(
        children: <Widget>[
          Padding(
         padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            // The first text field will be focused as soon as the app starts
            TextField(
              decoration: InputDecoration(
                labelText: 'Entre com seu Email!',                
              ),
              
              autofocus: true,
            ),
            Padding(padding: EdgeInsets.only(top: 30)),
            TextField(
              decoration: InputDecoration(
                labelText: 'Entre com sua Senha!'
              ),
              autofocus: true,
            ),
                        
          ],
        ),
          ),
          Padding(padding: EdgeInsets.only(top: 40)),
          RaisedButton.icon(
            textColor: Colors.white,
            color: Colors.lightGreen,
            icon: Icon(Icons.arrow_forward_ios), 
            label: Text("    Acessar    "), 
            onPressed: () {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => TelaInicial()),
              );
            },
          ),
           RaisedButton.icon(
            textColor: Colors.white,
            color: Colors.brown,
            icon: Icon(Icons.tablet_mac), 
            label: Text("Criar Conta   "), 
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CadastreWidget()),
              );
            },
          ),
           
        ],
      );
    }

    return Column(
      
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          alignment: Alignment.topCenter,
          child:  Text('Seja bem Vindo(a)'),
          height: 20,
          ),
      
        Image.network('https://alvo.com.br/store/image/catalog/Logos/Logo%20Web%20cópia.jpg',
        ),
        
        Container(
          alignment: Alignment.center,
          child:  Text('Faça login com sua conta ou crie !'),
          height: 120,
          ),
        _bottoes()
      ],
    );
  }
}