import 'package:alvo_couro/src/pages/detalhe-catalogo/detalhe-catalogo-widget.dart';
import 'package:flutter/material.dart';
import 'package:alvo_couro/src/pages/catalogo/catalogos-service.dart';
import 'package:alvo_couro/src/pages/catalogo/CatalogosModel.dart';
import 'dart:convert';

class CatalogoWidget extends StatefulWidget {
  _CatalogoWidgetState createState() => _CatalogoWidgetState();
}

class _CatalogoWidgetState extends State<CatalogoWidget>  with SingleTickerProviderStateMixin {
  TabController controller;
  final tabela = 'Products';
  final argumento = '';

   List<ProductModel> products = [];

  Future getData() async {
    API.getAllProducts(tabela, argumento).then((response) {
      
      var jsonBody = response['value'];
      for (var data in jsonBody) {
        if(jsonBody != 'undefined'){
            products.add(new ProductModel(
            data['Id'], data['Name'], data['ImageUrl']));
        }
        
      }
      setState(() {});
      products.forEach((somedata) => print("Nome : ${somedata.descricao}"));
    });
  }

  @override
  void initState() {
   super.initState();
    getData();
    controller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          'Catalogos',
          style: TextStyle(
              fontFamily: 'Montserrat',
              color: Colors.black,
              fontSize: 22.0,
              fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.camera),
            color: Colors.grey,
            onPressed: () {},
          )
        ],
      ),
       body: new Center(
        child: products.length == 0
            ? new Center(
                child: new CircularProgressIndicator(),
              )
            : showLista(),
      ),
    );
  }
     Widget showLista() {
    return new ListView.builder(
      itemCount: products.length,
      itemBuilder: (_, index) {
        return new Container(
          margin: new EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
          child: new Card(
            elevation: 10.0,
            child: new Container(
              padding: new EdgeInsets.all(12.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.symmetric(vertical: 3.0)),
                  new Text('ID: ${products[index].id}'),
                  new Padding(padding: new EdgeInsets.symmetric(vertical: 3.0)),
                  new Text('Nome: ${products[index].descricao}'),
                  new Padding(padding: new EdgeInsets.symmetric(vertical: 3.0)),
                  new Image(image: NetworkImage('${products[index].imagem}'),),
                  new Padding(padding: new EdgeInsets.symmetric(vertical: 3.0)),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
     /*  body: ListView(
        padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
        children: <Widget>[
          Container(
             child: products.length == 0
            ? new Center(
                child: new CircularProgressIndicator(),
              )
            : showLista(),
          ),
          Container(
            height: 150.0,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.all(10.0),
              children: <Widget>[
                listItem('assets/model1.jpeg', 'assets/chanellogo.jpg'),
                SizedBox(width: 35.0),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Material(
              borderRadius: BorderRadius.circular(15.0),
              elevation: 4.0,
              child: Container(
                height: 450.0,
                width: double.infinity,
                padding: EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          width: 50.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25.0),
                              image: DecorationImage(
                                  image: AssetImage('assets/model1.jpeg'),
                                  fit: BoxFit.cover)),
                        ),
                        SizedBox(width: 10.0),
                        Container(
                          width: MediaQuery.of(context).size.width - 120.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Daisy',
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 2.0),
                                  Text(
                                    '34 mins ago',
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 12.0,
                                        color: Colors.grey),
                                  ),
                                ],
                              ),
                              Icon(Icons.more_vert,
                                  color: Colors.grey, size: 20.0)
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 15.0),
                    Text(
                      'This official website features a ribbed knit zipper jacket that is modern and stylish. It looks very temparament and is recommended to friends',
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12.0,
                          color: Colors.grey),
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetalheCatalogoWidget(heroTag: 'assets/modelgrid1.jpeg')
                            ));
                          },
                          child: Hero(
                            tag: 'assets/modelgrid1.jpeg',
                            child: Container(
                              height: 200.0,
                              width:
                                  (MediaQuery.of(context).size.width - 50.0) /
                                      2,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  image: DecorationImage(
                                      image: AssetImage(
                                        'assets/modelgrid1.jpeg',
                                      ),
                                      fit: BoxFit.cover)),
                            ),
                          ),
                        ),
                        SizedBox(width: 10.0),
                        Column(
                          children: <Widget>[
                            InkWell(
                                onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => DetalheCatalogoWidget(heroTag: 'assets/modelgrid2.jpeg')
                                ));
                              },
                                child: Hero(
                                tag: 'assets/modelgrid2.jpeg',
                                child: Container(
                                  height: 95.0,
                                  width: (MediaQuery.of(context).size.width -
                                          100.0) /
                                      2,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/modelgrid2.jpeg'),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => DetalheCatalogoWidget(heroTag: 'assets/modelgrid3.jpeg')
                                ));
                              },  
                                child: Hero(
                                tag: 'assets/modelgrid3.jpeg',
                                child: Container(
                                  height: 95.0,
                                  width: (MediaQuery.of(context).size.width -
                                          100.0) /
                                      2,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.0),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/modelgrid3.jpeg'),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      height: 1.0,
                      width: double.infinity,
                      decoration:
                          BoxDecoration(color: Colors.grey.withOpacity(0.2)),
                    ),
                    ],
                ),
              ),
            ),
          )
        ],
      ), */
      /* bottomNavigationBar: Material(
        color: Colors.white,
        child: TabBar(
          controller: controller,
          indicatorColor: Colors.transparent,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.more, color: Colors.grey, size: 15.0)),
            Tab(icon: Icon(Icons.play_arrow, color: Colors.grey, size: 15.0)),
            Tab(icon: Icon(Icons.navigation, color: Colors.black, size: 15.0)),
            Tab(
                icon: Icon(Icons.supervised_user_circle,
                    color: Colors.grey, size: 15.0)),
          ],
        ), 
      ),
    );*/
  }

 /*  Widget listItem(String imgPath, String logo) {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
              height: 75.0,
              width: 75.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(37.5),
                  image: DecorationImage(
                      image: AssetImage(imgPath), fit: BoxFit.cover)),
            ),
            Positioned(
              top: 50.0,
              left: 50.0,
              child: Container(
                height: 25.0,
                width: 25.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.5),
                    image: DecorationImage(
                        image: AssetImage(logo), fit: BoxFit.contain)),
              ),
            )
          ],
        ),
        SizedBox(height: 10.0),
        Container(
          height: 30.0,
          width: 75.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Color(0xFF916144)),
          child: Center(
            child: Text(
              'Categoria',
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 14.0,
                  color: Colors.white),
            ),
          ),
        )
      ],
    );
  } 

  
}*/