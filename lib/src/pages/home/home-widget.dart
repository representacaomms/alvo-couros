import 'package:alvo_couro/src/pages/rastreamento/rastrearVendedor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        title: Text('Alvo Couros'),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.person),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.pin_drop),
            onPressed: () {
               Navigator.of(context).push(
                    new CupertinoPageRoute(builder: (BuildContext context) => new RastrearVendedor()));
            },
          ),
          // action button
          IconButton(
            icon: Icon(Icons.card_giftcard),
            onPressed: () {},
          ),
        ],
      ),
      

      // CONTEUDO DA PAGINA
      body: ListView(
        children: <Widget>[
          new Container(
            child: new Column(
              children: <Widget>[
                new Card(
                  child: new Column(
                    children: <Widget>[
                      new Image.network(
                          'https://alvo.com.br/store/image/catalog/Banner/016---Banner-Principal.jpg'),
                    ],
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Card(
                  child: new Column(
                    children: <Widget>[
                      new Image.network(
                          'https://alvo.com.br/store/image/catalog/Banner/014---o-PRESENTE-PERFEITO-E-O-PERSONALIZADO.jpg'),
                    ],
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Card(
                  child: new Column(
                    children: <Widget>[
                      new Image.network(
                          'https://alvo.com.br/store/image/catalog/Banner/015---Banner-Principal.jpg'),
                    ],
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Card(
                  child: new Column(
                    children: <Widget>[
                      new Image.network(
                          'https://scontent.fcpq2-1.fna.fbcdn.net/v/t1.0-9/47270475_2404398369574745_6190842392476647424_n.jpg?_nc_cat=102&_nc_ht=scontent.fcpq2-1.fna&oh=15edabc9e003d61cbacb5b87f5e532a7&oe=5CC020B4'),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
