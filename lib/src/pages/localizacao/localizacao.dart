import 'package:alvo_couro/src/pages/localizacao/widget-googleMaps.dart';
import 'package:flutter/material.dart';

class Localizacao extends StatefulWidget {
  _LocalizacaoState createState() => _LocalizacaoState();
}

class _LocalizacaoState extends State<Localizacao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WidgetGoogleMaps(),
    );
  }
}

